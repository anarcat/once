# ONCE

**once** is a very simple OATH secret keys manager and One Time Password 
generator written in bash. The OATH keys are stored in *~/.once-store* and 
encrypted using OpenPGP.

This utility provides commands to generate a TOTP Token for two factor 
authentication systems such as riseup.net (webmail), gandi.net, github.com, 
linode.com, wordpress blogs, etc. It also allows to add, remove, edit and sync 
the OATH secret keys needed to generate the One Time Password.

**once** is heavily based on Jasos Donenfeld's [Password Store](http://www.passwordstore.org). If
you are not using it, you might want to try it out!

More info at: https://antagonismo.org/code/once/ just not yet.

## Depends on:
- bash
  http://www.gnu.org/software/bash/
- GnuPG / GnuPG2
  http://www.gnupg.org/
- OATH Toolkit
  http://www.nongnu.org/oath-toolkit/
- git
  http://www.git-scm.com/
- xclip
  http://sourceforge.net/projects/xclip/
- tree >= 1.7.0
  http://mama.indstate.edu/users/ice/tree/
- GNU getopt
  http://www.kernel.org/pub/linux/utils/util-linux/
  http://software.frodo.looijaard.name/getopt/

## Migrating to/from pass-otp

This project is similar to the **[pass-otp](https://github.com/tadfisher/pass-otp)** extension. While **once**
is a standalone program, **pass-otp** is an extension to [Password
Store][] with support for generating qrcode images as well.

The storage and format of secrets in **pass-otp** is different from
**once**:

 * **once**: secret stored in `~/.once-store` in plaintext
 * **pass-otp**: secret stored alongside other **pass** secrets in
   `~/.password-store` in the [standard Key URI format](https://github.com/google/google-authenticator/wiki/Key-Uri-Format)

For example, to transfer from **once** to **pass-otp** you would do:

    $ once show ~/.once-store/example.com.gpg
    foo bar baz
    $ pass otp append example.com -s "foobarbaz"

Important things:

 * make sure to use the `append` command. `insert` will *replace* any
   existing normal password you might have already set for
   `example.com`
 * while **once** tolerates spaces in the secret, **pass-otp** does
   not and you need to remove those spaces before storing
 
In reverse, you'd need to extract the password from **pass-otp** and
insert it in **once**:

    $ pass show github.com  | grep ^otpauth:// | sed 's/^otpauth:.*secret=//;s/&.*$//'
    foobarbaz
    $ once insert github.com
